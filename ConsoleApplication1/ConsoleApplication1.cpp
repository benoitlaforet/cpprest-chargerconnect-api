// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "cpprest/containerstream.h"
#include "cpprest/filestream.h"
#include "cpprest/http_client.h"
#include "cpprest/json.h"
#include "cpprest/producerconsumerstream.h"
#include <iostream>
#include <sstream>
#include <array>


using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace ::pplx;
using namespace web::json;


string_t userToken;
string_t userTokenExpDateTime;

string_t tickTalkVersion = U("User");
string_t tickTalkReleaseNo = U("1.1.1");
const string_t host = U("http://localhost:3000");


int HTTPAsyncDonwloadFile(const method& mtd, const string_t host, const string_t api, std::vector<std::vector<string_t>> queryParams, const string_t saveFilename) {
	auto fileStream = std::make_shared<ostream>();

	//WORKING
	// Open stream to output file.
	pplx::task<void> requestTask = fstream::open_ostream(saveFilename).then([=](ostream outFile)
	{
		*fileStream = outFile;
		http_client client(host);
		// Build request URI and start the request.
		uri_builder builder(api);

		int size = queryParams.size();
		if (size > 0) {
			for (int row = 0; row < size; row++) {
				int colSize = queryParams[row].size();
				if (colSize = 2) {
					builder.append_query(queryParams[row][0], queryParams[row][1]);
				}
			}
		}
		auto query = builder.to_string();
		std::wcout << query << "\n";
		return client.request(mtd, builder.to_string());
	}).then([=](http_response response)	{
		printf("Received response status code:%u\n", response.status_code());
		return response.body().read_to_end(fileStream->streambuf());
		
	}).then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
	return 0;
}


pplx::task<void> HTTPAsync(const method& mtd, const string_t host, const string_t api, std::vector<std::vector<string_t>> queryParams, std::function<int(json::value obj)> functionToExecute)
{
	std::wcout << L"Calling HTTPAsync..." << std::endl;
	// Create http_client to send the request.
	http_client client(host);


	// Build request URI and start the request.
	uri_builder builder(api);

	
	int size = queryParams.size();
//	std::wcout << "size:" << size << "\n";

	if (size > 0) {
		for (int row = 0; row < size; row++) {
			int colSize = queryParams[row].size();
			if (colSize = 2) {
				builder.append_query(queryParams[row][0], queryParams[row][1]);
			}
		}
	}
	
//	auto query = builder.to_string();
//	std::wcout << query << "\n";

	return client.request(mtd, builder.to_string()).then([=](http_response response)
	{
		
		auto body = response.extract_string().get();

		if (response.status_code() == 200) {
			
			json::value obj = json::value::parse(body);
			functionToExecute(obj);
		}
		else {
			printf("HTTPAsync Received response status code:%u\n", response.status_code(), "\n");
			std::wcout << body;
		}
		
		

	});

}



pplx::task<void> HTTPAsyncSendFile(const method& mtd, const string_t host, const string_t api, std::vector<std::vector<string_t>> queryParams, std::function<int(json::value obj)> functionToExecute, const string_t fileToSend )
{
	std::wcout << L"Calling HTTPAsyncSendFile..." << std::endl;
	// Create http_client to send the request.
	http_client client(host);
	

	// Build request URI and start the request.
	uri_builder builder(api);
	int size = queryParams.size();
	if (size > 0) {
		for (int row = 0; row < size; row++) {
			int colSize = queryParams[row].size();
			if (colSize = 2) {
				builder.append_query(queryParams[row][0], queryParams[row][1]);
			}
		}
	}

	auto query = builder.to_string();
	std::wcout << query << "\n";
	auto fileStream = fstream::open_istream(fileToSend).get();
	if (!fileStream) {
		throw std::runtime_error("Can not open file");
	}

	else {
		//fileParams[utility::conversions::to_string_t("up_file")] = *upFile;
		
		return client.request(mtd, builder.to_string(), fileStream, _XPLATSTR("application/octet-stream")).then([=](http_response response)
		{

			auto body = response.extract_string().get();

			if (response.status_code() == 200) {

				json::value obj = json::value::parse(body);
				functionToExecute(obj);
			}
			else {
				printf("HTTPAsync Received response status code:%u\n", response.status_code(), "\n");
				std::wcout << body;
			}



		});
	}
	

}


int printObjectValues(json::value obj) {
	auto dataObj = obj.as_object();
	for (auto iterInner = dataObj.cbegin(); iterInner != dataObj.cend(); ++iterInner)
	{
		auto &propertyName = iterInner->first;
		auto &propertyValue = iterInner->second;

		std::wcout << propertyName << ": " << propertyValue << std::endl;
	}
	return 0;
}

int APIuserLogin(json::value obj) {
	std::wcout << "Token->" << obj[U("resp_data")][U("token")] << "\n";
	std::wcout << "Expiration Date->" << obj[U("resp_data")][U("expires")] << "\n";
	userToken = obj[U("resp_data")][U("token")].as_string();
	userTokenExpDateTime = obj[U("resp_data")][U("expires")].as_string();
	
	return 0;
}

int APITickVerify(json::value obj) {
	//json::value obj = json::value::parse(body);
	//std::wcout << "obj resp_data->" << obj[U("resp_data")];
	std::wcout << "tick_exist->" << obj[U("resp_data")][U("tick_exist")] << "\n";
	std::wcout << "tick id->" << obj[U("resp_data")][U("tick_id")] << "\n";
	//json::value objRespData = obj[U("resp_data")]
	//objRespData
	return 0;
}

int APITickTalkVerifyNewVersion(json::value obj) {
	//json::value obj = json::value::parse(body);
	//std::wcout << "obj resp_data->" << obj[U("resp_data")];

	/*
	Data structure for 200 results
	{
		"resp_data": {
			"data_rows": [
			{
				"id": 57,
					"idimport" : 192,
					"idsoftware" : 1,
					"software" : "TickTalk",
					"version" : "User",
					"idsoftwareversion" : 3,
					"release_no" : "1.33.37",
					"dttime" : "2020-01-10T18:42:21.257Z",
					"dt_release" : "2020-01-10T07:00:00.000Z",
					"cmt" : "<p>asd</p>",
					"v_1" : 1,
					"v_2" : 33,
					"v_3" : 37,
					"originalname" : "TickTalkInstall_USER_1.36.98_AAA (1).msi"
			}
			]
		}
	}
	*/
	
	auto arrayRelease = obj[U("resp_data")][U("data_rows")].as_array();
	int size = arrayRelease.size();
	
	
	std::wcout << "Row count:" << size << "\n" ;
	/* 
	//print object properties
	for (int row = 0; row < size; row++) {
		printObjectValues(arrayRelease[row]);
	}
	*/
	if (size > 0) {
		auto dataObj = arrayRelease[0].as_object();
		printObjectValues(arrayRelease[0]);
		if (dataObj[U("release_no")].as_string() != tickTalkReleaseNo) {
			string_t api = U("/openapi/software/release_file/ticktalk");

			std::vector<std::vector<string_t>> queryParams;
			queryParams.clear();
			queryParams.push_back({ U("token"), userToken });
			
			queryParams.push_back({ U("id_release"), std::to_wstring(dataObj[U("id")].as_integer()) });
			queryParams.push_back({ U("id_import"), std::to_wstring(dataObj[U("idimport")].as_integer()) });
			HTTPAsyncDonwloadFile(methods::GET, host, api, queryParams, dataObj[U("originalname")].as_string());
			queryParams.clear();
			
		}
	}

	return 0;
}

int APIAfterCBXUpload(json::value obj) {

	return 0;
}
int main(int argc, char* argv[])
{
	
	string_t api ;
	std::vector<std::vector<string_t>> queryParams;

	// SignIn ################
	
	api = U("/openapi/token/signin");
	queryParams.clear();
	queryParams.push_back({ U("u"), U("benoit.laforet@hotmail.com") });
	queryParams.push_back({ U("p"), U("Qwerty@1234") });
	HTTPAsync(methods::POST, host, api , queryParams, APIuserLogin).wait();
	queryParams.clear();


	/*
	// VERITY TICK ################
	api = U("/openapi/tick/verify");
	queryParams.clear();
	queryParams.push_back({ U("token"), userToken });
	queryParams.push_back({ U("tick_guid"), U("CC4656DB-19B5-4543-80E2-3CB25935E6A8") });
	HTTPAsync(methods::GET, host, api, queryParams, APITickVerify).wait();
	*/


	// Veriry Tick Talk New Release ################  replace ticktalk with the desired software ex: tickbox, ticktalk-manual ...
	/*api = U("/openapi/software/new_release/ticktalk");
	queryParams.clear();
	queryParams.push_back({ U("token"), userToken });
	queryParams.push_back({ U("hw_id"), U("CC4656DB-19B5-4543-80E2-3CB25935E6A8") });
	queryParams.push_back({ U("version"), tickTalkVersion });
	queryParams.push_back({ U("release_no"), tickTalkReleaseNo });
	HTTPAsync(methods::GET, host, api, queryParams, APITickTalkVerifyNewVersion).wait();
	*/

	// send CBX file ################  
	/*api = U("/openapi/import/cbx");
	queryParams.clear();
	queryParams.push_back({ U("token"), userToken });
	HTTPAsyncSendFile(methods::POST, host, api, queryParams, APIAfterCBXUpload, U("c:/Dev/190080792A.cbx")).wait();
	
	queryParams.clear();*/
	/*
	auto apiClient = std::make_shared<io::swagger::client::api::ApiClient>();
	auto apiConfig = std::make_shared<io::swagger::client::api::ApiConfiguration>();
	
	apiConfig->setBaseUrl(L"http://localhost:300");
	apiClient->setConfiguration(apiConfig);

	auto api = io::swagger::client::api::DefaultApi(apiClient);
	api.searchTablesGet().then([=](pplx::task<std::shared_ptr<io::swagger::client::model::RespTables>> tablelist) {
	*/
	return 0;
}


